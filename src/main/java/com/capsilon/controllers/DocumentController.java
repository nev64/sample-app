package com.capsilon.controllers;

import com.capsilon.models.Document;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RequestMapping(path = "/docs", produces = "application/json")
@RestController
public class DocumentController {

    private ArrayList<Document> documents;

    public DocumentController() {
        documents = new ArrayList<>();
        documents.add(new Document(1, "doc1"));
        documents.add(new Document(2, "doc2"));
    }

    @GetMapping()
    public Iterable<Document> getAll() {
        return new ArrayList<>(documents);
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<Document> getById(@PathVariable("id") int id) {
        return documents.stream().filter(d -> d.getId() == id).findFirst()
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

}
